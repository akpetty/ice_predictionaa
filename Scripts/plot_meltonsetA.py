############################################################## 
# Date: 01/01/16
# Name: plot_meltonset.py
# Author: Alek Petty

import matplotlib
matplotlib.use("AGG")
import pred_funcs as pfuncs
from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from scipy.io import netcdf
import numpy.ma as ma
from matplotlib import rc
from glob import glob
import matplotlib.patches as patches
from scipy.interpolate import griddata
import pandas as pd
from scipy import stats
import forecast_funcs as ff

def plot_meltonset(xpts, ypts, Melt_onset_year, year):
	textwidth=4.
	fig = figure(figsize=(textwidth,textwidth))
	subplots_adjust(bottom=0.01, top=0.99, left=0.01, right=0.99)

	#ax1=subplot(1, 3, 1)
	minval=50
	maxval=200
	#Melt_onset_year = ma.masked_where(~np.isfinite(Melt_onset_year), Melt_onset_year)

	#ADD GRIDSIZE=NUMBER KWARG TO HEXBIN IF YOU WANT TO CHANGE SIZE OF THE BINS
	im1 = m.pcolormesh(xpts , ypts, Melt_onset_year, cmap=viridis, vmin=minval, vmax=maxval,shading='flat', zorder=2)
	#im2 = m.contour(xpts , ypts, ma.mean(Pressure, axis=0),levels=[990, 1000, 1100],colors='k', zorder=4)
	m.drawcoastlines(linewidth=0.5, zorder=5)
	m.drawparallels(np.arange(90,-90,-10), linewidth = 0.25, zorder=3)
	m.drawmeridians(np.arange(-180.,180.,30.), linewidth = 0.25, zorder=3)
	#m.plot(xptsR, yptsR, '--', linewidth = 2, color='k', zorder=5)

	#ADD COLORBAR TO MAP
	#bbox_args = dict(fc="white")
	#ax1.annotate('.                   \n             \n        ', xy=(0.02, 0.98), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='left', verticalalignment='top', zorder=10)

	#ax1.annotate(files[x][-8:-4]+'-'+files[x][-4:-2]+'-'+files[x][-2:], xy=(0.98, 0.98), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='right', verticalalignment='top', zorder=10)
	label_str='Day'
	#ax1.annotate('AIRS temp anomaly from 2003-2014 mean', xy=(0.02, 0.02), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='left', verticalalignment='bottom', zorder=10)
	cax = fig.add_axes([0.02, 0.88, 0.25, 0.035])
	cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
	cbar.set_label(label_str, labelpad=1)
	cbar.set_ticks([minval, maxval])
	cbar.solids.set_rasterized(True)
	#SHIFT COLOR SPACE SO OFF WHITE COLOR IS AT 0 m
	#cbar.set_clim(minval, maxval)
	savefig(figpath+'MELT_ONSET/meltonset'+'_'+str(year)+var_str+'.png', dpi=300)
	close(fig)

viridis=pfuncs.get_new_cmaps(cmap_str='viridis')
rcParams['axes.labelsize'] = 9
rcParams['xtick.labelsize']=9
rcParams['ytick.labelsize']=9
rcParams['legend.fontsize']=9
rcParams['font.size']=9
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})

m = Basemap(projection='npstere',boundinglat=65,lon_0=0, resolution='l'  )

datapath = '../../../DATA/MELT_SEASON/'
rawdatapath='../../../DATA/'
dataoutpath='./Data_output/MELT_OUT/'
figpath='./Figures/'

start_year=1979
end_year=2016


var_str='meltN_nan'
xpts, ypts, Melt_onset_years=ff.get_onset_gridded(dataoutpath, start_year, end_year, var_str)
#var_str='alek_melt_3'
#xpts, ypts, lons, lats, Melt_onset_years =pfuncs.get_meltonset(m, rawdatapath, var_str, 
#	str(start_year), str(end_year), masked=1)
#melt_day=150
#Melt_onset_years=melt_day-Melt_onset_years
#Melt_onset_years[where(Melt_onset_years<0)]=0

#AREA WEIGHT THE VALUES..
areaF=reshape(fromfile(file=open(rawdatapath+'/OTHER/psn25area_v3.dat', 'rb'), 
	dtype='<i4')/1000., [448, 304])
areaF=areaF/np.amax(areaF)


#Melt_onset_years=ma.masked_where((Melt_onset_years<-0.5), Melt_onset_years)
mean = np.ma.average(Melt_onset_years, axis=(1,2))
print mean


#a=Melt_onset_years[0]
#Melt_onset_years=Melt_onset_years.astype(float)
#a[where(a<-0.5)]=np.nan

for year in xrange(start_year, end_year+1, 1):
	print year
	plot_meltonset(xpts, ypts, Melt_onset_years[year-start_year], year)



