""" 
 calc_Antarcticforecast.py
 Date: 02/03/2017
 Author: Alek Petty
 Description: Script to generate Antarctic sea ice extent forecast using a given variable (or multiple variables)

 Run with e.g. python -i calc_get_multivar_forecast_months.py 
 If you want to run with options, uncomment out the parser lines, then run as:
 python -i calc_get_multivar_forecast_months.py  --fmonth 6 --pmonth 9 --fvars conc --weight 1
 NB if you want plot_forecast to run, copy this in from forecast_funcs.py

"""
import matplotlib
matplotlib.use("AGG")

import argparse
import sys
import forecast_funcs as ff
from pylab import *

figpath='../Figures/'
skilldatapath='../DataOutput/SkillVals/'
linedatapath='../DataOutput/TimeSeries/'
rawdatapath='../../../../DATA/'
datapath='../DataOutput/'
extdatapath = datapath+'/Extent/'

# Options for this Script
plotSkill=1
outSkill=1
outLine=1
outWeights=1


startYear=1979 # Start of forecast training
endYear=2016
startYearPred=1985 # Start of forecasts

yearsP=arange(startYearPred, endYear+1, 1)
numYearsReq=5 #number of years required in a grid cell for it to count towards the training data

# Defaults if no arguments added after the call.
fmonth=6 #6=June, 9=Sep
pmonth=9 #9=SEP
fvars=['conc']
weight=1 # Spatially weighting the data
hemStr='N'
iceType='area'


region=0
#0 implies pan-Arctic or Antarctic
#2 Weddell Sea
#3 Indian Ocean
#4 Pacific Ocean
#5 Ross Sea
#6 Amundsen/BHausen Sea

#parser = argparse.ArgumentParser()
#parser.add_argument('--fmonth', type=int)
#parser.add_argument('--pmonth', type=int)
#parser.add_argument('--fvars', nargs='+')
#parser.add_argument('--weight', type=int)
#parser.add_argument('--numVars', type=int)
#args=parser.parse_args()
#fvars=args.fvars
#weight=args.weight
#fmonth=args.fmonth
#pmonth=args.pmonth

varStrsOut=''.join(fvars)

print 'fmonth:', fmonth, 'pmonth:', pmonth
print fvars
print 'Weighted:', weight


# Load empty lists (not very Pythony)
extentObsDt=[] # Detrended (observed) ice extent from linear trend persistence (LTP)
extentPredDt=[] # Detrended (forecast) ice extent from LTP
extentPredAbs=[] # Aboslute forecast of sea ice extent (add LTP)
anoms=[] # Forecast anomaly from observed
perr=[] # Estimated forecast error (1 SD)

# Generate a new forecast each year
for year in xrange(startYearPred, endYear+1, 1):
	if (year<2017):
		vals=ff.CalcForecastMultiVar(year, startYear, fvars, fmonth, month=pmonth,
			region=region, anomObs=1, numYearsReq=numYearsReq, weight=weight, outWeights=outWeights, 
			icetype=iceType, hemStr=hemStr, siiVersion='v2.1')
		extentObsDt.append(vals[0])
		extentPredDt.append(vals[1])
		extentPredAbs.append(vals[2])
		anoms.append(vals[3])
		perr.append(vals[4])
		print 'Forecast year:', year, fvars, 'DT SIE forecast:', vals[1], 'DT SIE Obs :', vals[0],'SIE forecast:', vals[2] 
		


rmsF= ff.rms(anoms)
rmsP= ff.rms(extentObsDt)
errorFore = '%.2f' %sqrt(rmsF)
errorExt = '%.2f' %sqrt(rmsP)
skill = '%.2f' %(1 - (rmsF/rmsP))
skill2 = '%.2f' %(1 - (ff.rms(anoms[-9:]))/(ff.rms(extentObsDt[-9:])))
#skill3 = '%.2f' %(1 - (rms(anoms[-16:-8]))/(rms(extentObsDt[-16:-8])))

print '1985-2016 skill:',skill, '2008-2016 skill:', skill2

if (outSkill==1):
	savetxt(skilldatapath+'Skill_'+varStrsOut+'fm'+str(fmonth)+'pm'+str(pmonth)
		+str(startYearPred)+str(endYear)+'W'+str(weight)+iceType+'.txt', 
		array([skill, errorFore, errorExt, skill2])[None], 
		header='skill error_forr error_obs skill2', fmt='%s')

if (outLine==1):
	array(extentPredDt).dump(linedatapath+iceType+'PredDt'
		   +varStrsOut+'fm'+str(fmonth)+'pm'+str(pmonth)+str(startYearPred)+str(endYear)
		   +'W'+str(weight)+'.txt')
	array(extentObsDt).dump(linedatapath+iceType+'ObsDt'
		   +varStrsOut+'fm'+str(fmonth)+'pm'+str(pmonth)+str(startYearPred)+str(endYear)
		   +'W'+str(weight)+'.txt')
	array(extentPredAbs).dump(linedatapath+iceType+'PredAbs'
		   +varStrsOut+'fm'+str(fmonth)+'pm'+str(pmonth)+str(startYearPred)+str(endYear)
		   +'W'+str(weight)+'.txt')


if (region==0):
	# GET extent AND DETREND
	years, extent = ff.get_ice_extentN(rawdatapath, pmonth, startYear, endYear, icetype=iceType, version='v2.1',  hemStr=hemStr)
else: 
	if (hemStr=='N'):
		poleStr=''
	elif (hemStr=='S'):
		poleStr='AA'
# If generating a regional forecast
	extentT=loadtxt(extdatapath+'ice_'+iceType+'_M'+str(pmonth)+'R'+str(region)+'_19792016'+poleStr)
	#get years and extent for years preceeding the given forecast year
	years=np.arange(startYear, endYear+1, 1)
	extent=extentT[0:endYear-startYear+1]


if (plotSkill==1):
	ff.plot_forecast(years, extent, yearsP, extentPredAbs, extentObsDt, extentPredDt, perr, errorFore, errorExt, skill2, varStrsOut, fmonth, pmonth, weight, figpath, hemStr, iceType, region)



