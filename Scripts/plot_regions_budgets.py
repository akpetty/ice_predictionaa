############################################################## 
# Date: 01/01/16
# Name: plot_meltonset.py
# Author: Alek Petty

import matplotlib
matplotlib.use("AGG")

from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from matplotlib import rc
import forecast_funcs as ff

rcParams['axes.labelsize'] = 9
rcParams['xtick.labelsize']=9
rcParams['ytick.labelsize']=9
rcParams['font.size']=9
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})

m = Basemap(projection='npstere',boundinglat=52,lon_0=0, resolution='l'  )

data_path = '../../../../DATA/'

figpath='../Figures/'

#viridis=pfuncs.get_new_cmaps(cmap_str='viridis')



region_mask, xpts, ypts = ff.get_region_mask_sect(data_path, m, xypts_return=1)
region_mask=ma.masked_where(region_mask==1.5, region_mask)
region_mask=ma.masked_where(region_mask>19.5, region_mask)

regions=np.ones((region_mask.shape))

regions[where(region_mask==15)]=2
regions[where((region_mask==7)|(region_mask==8))]=3
regions[where((region_mask==9)|(region_mask==10)|(region_mask==11))]=4
regions[where((region_mask==3)|(region_mask==12)|(region_mask==13))]=5

regions=ma.masked_where(regions<2, regions)

	# ;           = 2   Sea of Okhotsk and Japan
	# ;           = 3   Bering Sea
	# ;           = 4   Hudson Bay
	# ;           = 5   Gulf of St. Lawrence
	# ;           = 6   Baffin Bay/Davis Strait/Labrador Sea
	# ;           = 7   Greenland Sea
	# ;           = 8   Barents Seas
	# ;           = 9   Kara
	# ;           =10   Laptev
	# ;           =11   E. Siberian
	# ;           =12   Chukchi
	# ;           =13   Beaufort
	# ;           =14   Canadian Archipelago
	# ;           =15   Arctic Ocean
	# ;           =20   Land
	# ;           =21   Coast

	# ;           = 7   Greenland Sea
	# ;           = 8   Barents Seas
	# ;           = 9   Kara
	# ;           =10   Laptev
	# ;           =11   E. Siberian
	# ;           =12   Chukchi
	# ;           =13   Beaufort

#region=10
#region_mask=ma.masked_where(region_mask!=region, region_mask)

textwidth=4.
fig = figure(figsize=(textwidth,textwidth))
subplots_adjust(bottom=0.01, top=0.99, left=0.01, right=0.99)

#ax1=subplot(1, 3, 1)
minval=2
maxval=5
#ADD GRIDSIZE=NUMBER KWARG TO HEXBIN IF YOU WANT TO CHANGE SIZE OF THE BINS
im1 = m.pcolormesh(xpts , ypts, regions, cmap=cm.jet, vmin=minval, vmax=maxval,shading='gouraud', zorder=2, alpha=0.8)
#im2 = m.contour(xpts , ypts, ma.mean(Pressure, axis=0),levels=[990, 1000, 1100],colors='k', zorder=4)
m.drawcoastlines(linewidth=0.5, zorder=5)
m.drawparallels(np.arange(90,-90,-10), linewidth = 0.25, zorder=3)
m.drawmeridians(np.arange(-180.,180.,30.), linewidth = 0.25, zorder=3)

#ax1.annotate(files[x][-8:-4]+'-'+files[x][-4:-2]+'-'+files[x][-2:], xy=(0.98, 0.98), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='right', verticalalignment='top', zorder=10)
label_str='Region'
#ax1.annotate('AIRS temp anomaly from 2003-2014 mean', xy=(0.02, 0.02), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='left', verticalalignment='bottom', zorder=10)
#cax = fig.add_axes([0.02, 0.88, 0.25, 0.035])
#cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
#cbar.set_label(label_str, labelpad=1)
#cbar.set_ticks(np.arange(minval, maxval+1, 4))
#cbar.solids.set_rasterized(True)
#SHIFT COLOR SPACE SO OFF WHITE COLOR IS AT 0 m
#cbar.set_clim(minval, maxval)
savefig(figpath+'/regionsArctic.png', dpi=300)
#savefig(figpath+'/regions_'+str(region)+'.png', dpi=300)
close(fig)
