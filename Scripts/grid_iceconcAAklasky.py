############################################################## 
# Date: 01/01/17
# Name: grid_iceconc.py
# Author: Alek Petty
# Description: Grid the ice concentration data onto a coarser polar stereographic grid 

import matplotlib
matplotlib.use("AGG")
from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from scipy.io import netcdf
import numpy.ma as ma
from matplotlib import rc
from glob import glob
import matplotlib.patches as patches
from scipy.interpolate import griddata
import pandas as pd
from scipy import stats
import forecast_funcs as ff

from netCDF4 import Dataset

m = Basemap(projection='spstere',boundinglat=-52,lon_0=180, resolution='l'  )

datapath = 'C:/users/ryan/anaconda2/scripts/antarcticseaice/DATA/'
rawdatapath='C:/users/ryan/anaconda2/scripts/antarcticseaice/DATA/'
dataoutpath='C:/users/ryan/anaconda2/scripts/antarcticseaice/DataOutput/IceConcAA/'
figpath='C:/users/ryan/anaconda2/scripts/antarcticseaice/Figures/'

start_year=2016
end_year=2016

startMonth=11 #3=April, 7=August
endMonth=11

poleStr='AA'

dx_res = 100000.
nx = int((m.xmax-m.xmin)/dx_res)+1; ny = int((m.ymax-m.ymin)/dx_res)+1
grid_str=str(int(dx_res/1000))+'km'
lonsG, latsG, xptsG, yptsG = m.makegrid(nx, ny, returnxy=True)
xptsG.dump(dataoutpath+'xpts'+grid_str+poleStr)
yptsG.dump(dataoutpath+'ypts'+grid_str+poleStr)
print xptsG.shape
lats, lons = ff.get_psslatslons(datapath)
xpts, ypts =m(lons, lats)


alg=0 #0=Nasa team

#SSMIS 89.18 -	January 2008 to present
#SSM/I Pole Hole Mask	87.2	July 1987 through December 2007
#SMMR Pole Hole Mask - 84.5	November 1978 through June 1987

for month in xrange(startMonth, endMonth+1):
	print 'Month:', month
	for year in xrange(start_year, end_year+1):
		print year
		if (year>2015):
			
			ice_conc = ff.get_month_concSN_NRT(datapath, year, month, alg=alg, pole=poleStr, monthMean=1)
			

		else:
			ice_conc = ff.get_month_concSN(datapath, year, month, alg=alg, pole=poleStr)

		ice_conc=ma.masked_where(ice_conc<=0.15, ice_conc)
					
		ice_conc = ice_conc.filled(0)
		

		
		ice_concG = griddata((xpts.flatten(), ypts.flatten()),ice_conc.flatten(), (xptsG, yptsG), method='linear')
		ice_conc_ma=ma.masked_where(np.isnan(ice_concG), ice_concG)
		#ice_conc_ma=ma.masked_where((latsG>pmask), ice_conc_ma)
		ice_conc_ma.dump(dataoutpath+'ice_conc'+grid_str+str(month)+str(year)+poleStr)



