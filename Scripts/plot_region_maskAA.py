############################################################## 
# Date: 01/01/17
# Name: plot_iceconcAA.py
# Author: Alek Petty

import matplotlib
matplotlib.use("AGG")

from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from matplotlib import rc
import forecast_funcs as ff

rcParams['axes.labelsize'] = 9
rcParams['xtick.labelsize']=9
rcParams['ytick.labelsize']=9
rcParams['font.size']=9
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})


m = Basemap(projection='spstere',boundinglat=-52,lon_0=180, resolution='l'  )

dataPath = '../../../../DATA/'

figpath='../Figures/'


lats, lons = ff.get_psslatslons(dataPath)
areaF=reshape(fromfile(file=open(dataPath+'/OTHER/pss25area_v3.dat', 'rb'), dtype='<i4')/1000., [332, 316])/1e6
region_mask, xpts, ypts = ff.get_region_maskAA(dataPath, m, xypts_return=1)


region=6
#2 Weddell Sea
#3 Indian Ocean
#4 Pacific Ocean
#5 Ross Sea
#6 Amundsen/BHausen Sea
#11 Land
#12 Coast
regionStr=['0', '1', 'W', 'I', 'P', 'R', 'AB']



region_mask=ma.masked_where(region_mask==1.5, region_mask)
region_mask=ma.masked_where(region_mask>10, region_mask)


locLat=[-60, -60, -60, -60, -60]
locLon=[-30, 50, 130, 190, 260]
xptsLoc, yptsLoc=m(locLon, locLat)

#region=10
#region_mask=ma.masked_where(region_mask!=region, region_mask)

textwidth=4.
fig = figure(figsize=(textwidth,textwidth))
ax=gca()
subplots_adjust(bottom=0.01, top=0.99, left=0.01, right=0.99)

#ax1=subplot(1, 3, 1)
minval=1
maxval=7
#ADD GRIDSIZE=NUMBER KWARG TO HEXBIN IF YOU WANT TO CHANGE SIZE OF THE BINS
im1 = m.pcolormesh(xpts , ypts, region_mask, cmap=cm.viridis, vmin=minval, vmax=maxval,shading='gouraud', zorder=2)
#im2 = m.contour(xpts , ypts, ma.mean(Pressure, axis=0),levels=[990, 1000, 1100],colors='k', zorder=4)
m.drawcoastlines(linewidth=0.5, zorder=5)
m.drawparallels(np.arange(90,-90,-10), linewidth = 0.25, zorder=3)
m.drawmeridians(np.arange(-180.,180.,30.), linewidth = 0.25, zorder=3)

for x in xrange(5):
	ax.annotate(str(x+2), xy=(xptsLoc[x], yptsLoc[x]), zorder=10)

#ax1.annotate(files[x][-8:-4]+'-'+files[x][-4:-2]+'-'+files[x][-2:], xy=(0.98, 0.98), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='right', verticalalignment='top', zorder=10)
label_str='Region'
#ax1.annotate('AIRS temp anomaly from 2003-2014 mean', xy=(0.02, 0.02), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='left', verticalalignment='bottom', zorder=10)
cax = fig.add_axes([0.45, 0.45, 0.2, 0.035])
cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
cbar.set_label(label_str, labelpad=1)
cbar.set_ticks(np.arange(minval, maxval+1, 3))
cbar.solids.set_rasterized(True)
#SHIFT COLOR SPACE SO OFF WHITE COLOR IS AT 0 m
#cbar.set_clim(minval, maxval)
savefig(figpath+'/regionsAntarctic.png', dpi=300)
#savefig(figpath+'/regions_'+str(region)+'.png', dpi=300)
close(fig)
