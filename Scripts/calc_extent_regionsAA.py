############################################################## 
# Date: 10/05/16
# Name: calc_ice_extent_regionsAA.py
# Author: Alek Petty
# Description: Script to calculate Antarctic ice exent for various regions.

import matplotlib
matplotlib.use("AGG")
import sys
sys.path.append("../")

import numpy as np
from pylab import *
import numpy.ma as ma
from mpl_toolkits.basemap import Basemap, shiftgrid
from glob import glob
from netCDF4 import Dataset

dataPath = '../../../../DATA/'
outPath='../DataOutput/Extent/'
figPath='../Figures/'

m = Basemap(projection='npstere',boundinglat=66,lon_0=0, resolution='l'  )


lats, lons = ff.get_psslatslons(dataPath)
areaF=reshape(fromfile(file=open(dataPath+'/OTHER/pss25area_v3.dat', 'rb'), dtype='<i4')/1000., [332, 316])/1e6
region_mask, xpts, ypts = ff.get_region_maskAA(dataPath, m, xypts_return=1)




region=4
#2 Weddell Sea
#3 Indian Ocean
#4 Pacific Ocean
#5 Ross Sea
#6 Amundsen/BHausen Sea
#11 Land
#12 Coast
#regionStr=['ALL', '1', 'W', 'I', 'P', 'R', 'AB']


start_year=1979
end_year=2016
month = 8 # 9 = Sept
alg=0 #0=NASA TEAM, 1=BOOTSTRAP

ice_ext_mean=[]
ice_area_mean=[]


print 'Month:', month
for year in xrange(start_year, end_year+1):
	print year
	if (year>2015):
		# Subtract 1 to get month index starting from zero.
		ice_conc = ff.get_month_concSN_NRT(dataPath, year, month-1, alg=alg, pole='AA', monthMean=1, mask=1)
	else:
		ice_conc = ff.get_month_concSN(dataPath, year, month-1, alg=alg, pole='AA')
				
	ice_conc = ice_conc.filled(0)
	
	ice_conc = where((ice_conc <=0.15), 0, ice_conc)
	
	if (region>0):
		ice_conc = where((region_mask==region), ice_conc, 0)

	ice_area = ice_conc*areaF
	ice_ext = where((ice_conc >=0.15), 1, 0)*areaF
	

	ice_ext_mean.append(sum(ice_ext))
	ice_area_mean.append(sum(ice_area))

savetxt(outPath+'ice_extent_M'+str(month)+'R'+str(region)
+'_'+str(start_year)+str(end_year)+'AA', ice_ext_mean)

savetxt(outPath+'ice_area_M'+str(month)+'R'+str(region)
+'_'+str(start_year)+str(end_year)+'AA', ice_area_mean)


Years=np.arange(start_year, end_year+1, 1)

fig = figure(figsize=(5,2.2))
im1 = plot(Years, ice_ext_mean, 'k')
ylabel('Extent [M km2]')
savefig(figPath+'ice_extent_M'+str(month)+'R'+str(region)+'AA.png')


fig = figure(figsize=(5,2.2))
im1 = plot(Years, ice_area_mean, 'k')
ylabel('Area [M km2]')
savefig(figPath+'ice_area_M'+str(month)+'R'+str(region)+'AA.png')


