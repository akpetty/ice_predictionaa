############################################################## 
# Date: 
# Name: grid_meltonset.py
# Author: Alek Petty
# Description: Grid the Arctic melt onset data onto a coarser polar stereographic grid 

import matplotlib
matplotlib.use("AGG")
from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from scipy.io import netcdf
import numpy.ma as ma
from matplotlib import rc
from glob import glob
import matplotlib.patches as patches
from scipy.interpolate import griddata
import pandas as pd
from scipy import stats
from netCDF4 import Dataset
import forecast_funcs as ff


def plot_meltonset(xpts, ypts, meltOnsetYearT, year):
	textwidth=4.
	fig = figure(figsize=(textwidth,textwidth))
	subplots_adjust(bottom=0.01, top=0.99, left=0.01, right=0.99)

	#ax1=subplot(1, 3, 1)
	minval=50
	maxval=200
	#Melt_onset_year = ma.masked_where(~np.isfinite(Melt_onset_year), Melt_onset_year)

	#ADD GRIDSIZE=NUMBER KWARG TO HEXBIN IF YOU WANT TO CHANGE SIZE OF THE BINS
	im1 = m.pcolormesh(xpts , ypts, meltOnsetYearT, cmap=cm.viridis, vmin=minval, vmax=maxval,shading='flat', zorder=2)
	#im2 = m.contour(xpts , ypts, ma.mean(Pressure, axis=0),levels=[990, 1000, 1100],colors='k', zorder=4)
	m.drawcoastlines(linewidth=0.5, zorder=5)
	m.drawparallels(np.arange(90,-90,-10), linewidth = 0.25, zorder=3)
	m.drawmeridians(np.arange(-180.,180.,30.), linewidth = 0.25, zorder=3)
	
	label_str='Day'
	#ax1.annotate('AIRS temp anomaly from 2003-2014 mean', xy=(0.02, 0.02), bbox=bbox_args,xycoords='axes fraction', horizontalalignment='left', verticalalignment='bottom', zorder=10)
	cax = fig.add_axes([0.02, 0.88, 0.25, 0.035])
	cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
	cbar.set_label(label_str, labelpad=1)
	cbar.set_ticks([minval, maxval])
	cbar.solids.set_rasterized(True)
	#SHIFT COLOR SPACE SO OFF WHITE COLOR IS AT 0 m
	#cbar.set_clim(minval, maxval)
	savefig(figpath+'/meltonset'+'_'+str(year)+var+out_str+poleStr+'.png', dpi=300)
	close(fig)


m = Basemap(projection='npstere',boundinglat=65,lon_0=0, resolution='l'  )

rawdatapath='../../../../DATA/'
dataoutpath='../DataOutput/MeltOut/'
figpath='../Figures/MeltOnset/'

start_year=1979
end_year=2017

poleStr='A'

dx_res = 100000.
nx = int((m.xmax-m.xmin)/dx_res)+1; ny = int((m.ymax-m.ymin)/dx_res)+1
grid_str=str(int(dx_res/1000))+'km'
lons_100km, lats_100km, xpts100, ypts100 = m.makegrid(nx, ny, returnxy=True)

#shift by half a grid cell
xpts100=xpts100-(dx_res/2)
ypts100=ypts100-(dx_res/2)

xpts100.dump(dataoutpath+'xpts'+grid_str+poleStr)
ypts100.dump(dataoutpath+'ypts'+grid_str+poleStr)

openwaternan=0
if (openwaternan==1):
	openwaterval=np.nan
	out_str='melt_nan'
else:
	openwaterval=75
	out_str='melt'

var='730smelt'	


for year in xrange(start_year, end_year+1, 1):


	print year

	xpts, ypts, lons, lats, meltOnsetYearT= ff.get_meltonset(m, 
		rawdatapath, 'MELTFREEZE', var, year, openwaterval=openwaterval, masked=0)
	
	meltOnsetYearG = griddata((xpts.flatten(), ypts.flatten()),meltOnsetYearT.flatten(), (xpts100, ypts100), method='linear')

	meltOnsetYearGma = ma.masked_where(~np.isfinite(meltOnsetYearG), meltOnsetYearG)


	plot_meltonset(xpts100, ypts100, meltOnsetYearGma, year)

	meltOnsetYearGma.dump(dataoutpath+out_str+grid_str+str(year)+poleStr)



