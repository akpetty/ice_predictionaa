############################################################## 
# Date: 01/04/17
# Name: plotSkillHeat.py
# Author: Alek Petty
# Description: Script to plot skill heat map

import matplotlib
matplotlib.use("AGG")

from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from matplotlib import rc


rcParams['xtick.major.size'] = 2
rcParams['ytick.major.size'] = 2
rcParams['axes.linewidth'] = .5
rcParams['lines.linewidth'] = .5
rcParams['patch.linewidth'] = .5
rcParams['axes.labelsize'] = 8
rcParams['xtick.labelsize']=8
rcParams['ytick.labelsize']=8
rcParams['legend.fontsize']=8
rcParams['font.size']=8
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})


skilldatapath='../DataOutput/'
#dataoutpathC='./Data_output/CONC_OUT/'
figpath='../Figures/'

skillE = loadtxt(skilldatapath+'Extent.csv', skiprows=1, delimiter=',')
skillA = loadtxt(skilldatapath+'Area.csv', skiprows=1, delimiter=',')

minval=-0.5
maxval=0.5

minvalD=-0.2
maxvalD=0.2

fig = figure(figsize=(6,3))
ax1=subplot(1, 3, 1)
im1 = pcolormesh(skillE[:, 1:], vmin=minval, vmax=maxval, cmap=cm.RdBu_r)
ax1.set_xticks(np.arange(0.5, 4, 1))
ax1.set_xticklabels(np.arange(9, 13, 1))
ax1.set_yticks(np.arange(0.5, 6, 1))
ax1.set_yticklabels(['AA', '2', '3', '4', '5', '6'])
ax1.set_ylabel('Region')
ax1.xaxis.tick_top()
ax1.invert_yaxis()

cax = fig.add_axes([0.11, 0.1, 0.2, 0.02])
cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
cbar.set_label('Extent skill',labelpad=3)
cbar.set_ticks([minval, 0, maxval])


ax2=subplot(1, 3, 2)
im2 = pcolormesh(skillA[:, 1:], vmin=minval, vmax=maxval, cmap=cm.RdBu_r)
ax2.set_xticks(np.arange(0.5, 4, 1))
ax2.set_xticklabels(np.arange(9, 13, 1))


ax2.set_yticklabels([])
ax2.xaxis.tick_top()
ax2.invert_yaxis()
ax2.set_xlabel('Forecasted month')
ax2.xaxis.set_label_position('top') 

cax2 = fig.add_axes([0.43, 0.1, 0.2, 0.02])
cbar2 = colorbar(im2,cax=cax2, orientation='horizontal', extend='both', use_gridspec=True)
cbar2.set_label('Area skill',labelpad=3)
cbar2.set_ticks([minval, 0, maxval])


ax3=subplot(1, 3, 3)
im3 = pcolormesh(skillA[:, 1:]-skillE[:, 1:], vmin=minvalD, vmax=maxvalD, cmap=cm.RdBu_r)
ax3.set_xticks(np.arange(0.5, 4, 1))
ax3.set_xticklabels(np.arange(9, 13, 1))
ax3.set_yticklabels([])
ax3.xaxis.tick_top()
ax3.invert_yaxis()

cax3 = fig.add_axes([0.75, 0.1, 0.2, 0.02])
cbar3 = colorbar(im3,cax=cax3, orientation='horizontal', extend='both', use_gridspec=True)
cbar3.set_label('Area-Extent skill',labelpad=3)
cbar3.set_ticks([minvalD, 0, maxvalD])


subplots_adjust(left=0.08, right=0.98, top=0.88, bottom=0.13, wspace=0.1)
savefig(figpath+'skillHeatMap.png', dpi=300)
close(fig)







