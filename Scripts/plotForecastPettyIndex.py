############################################################## 
# Date: 01/01/17
# Name: plot_iceconcAA.py
# Author: Alek Petty

import matplotlib
matplotlib.use("AGG")

from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from matplotlib import rc
import forecast_funcs as ff


rcParams['xtick.major.size'] = 2
rcParams['ytick.major.size'] = 2
rcParams['axes.linewidth'] = .5
rcParams['lines.linewidth'] = .5
rcParams['patch.linewidth'] = .5
rcParams['axes.labelsize'] = 8
rcParams['xtick.labelsize']=8
rcParams['ytick.labelsize']=8
rcParams['legend.fontsize']=7
rcParams['font.size']=7
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

rawdatapath='../../../../DATA/'
figpath='../Figures/'
skilldatapath='../DataOutput/Arctic/SkillVals/'
linedatapath='../DataOutput/Arctic/TimeSeries/'
weightdatapath='../DataOutput/Arctic/Weights/'
extdatapath='../DataOutput/Extent/'

pmonth=9 #SEP
startYear=1979
endYear=2017
startYearPred=1985
yearsP=np.arange(startYearPred, endYear+1, 1)

fmonth=7 #6=june

iceType='extent'
hemStr='N'
#varStrsOut=['conc', 'melt', 'pond']
varStr='conc'
region=0

if (region==0):

	years, extent = ff.get_ice_extentN(extdatapath, pmonth, startYear, endYear, icetype=iceType, version='v2.1',  hemStr=hemStr)
elif (region==-1):

	years, extent = ff.getIceExtentAreaPetty(extdatapath, pmonth, startYear, endYear, icetype=iceType, alg=0)


if ((region<1)&(iceType=='area')):
	ymin=2
	ymax=6
	yminAnom=-2
	ymaxAnom=2
	regionOut='Arctic'
elif ((region=='A')):
	ymin=0
	ymax=2
	yminAnom=-1
	ymaxAnom=1
	regionOut='Alaska'
	skilldatapath='../DataOutput/Alaska/SkillVals/'
	linedatapath='../DataOutput/Alaska/TimeSeries/'
	weightdatapath='../DataOutput/Alaska/Weights/'
	poleStr='A'
	extent=loadtxt(extdatapath+'ice_'+iceType+'_M'+str(pmonth)+'RA_19792016'+poleStr)

	years=np.arange(startYear, endYear, 1)

else:
	ymin=3
	ymax=8
	yminAnom=-1
	ymaxAnom=1
	regionOut='Arctic'

if (pmonth==10):
	ymin=5
	ymax=10

outNum=1


# GET extent AND DETREND
#years, extent = ff.get_ice_extentN(extdatapath, pmonth, startYear, endYear, icetype=iceType, version='v2.1',  hemStr=hemStr)
	#years=np.arange(startYear, endYear+1, 1)


#extentDT, lineT=pfuncs.get_varDT(years, extent)
extentPredDt=[]
extentObsDt=[]
extentPredAbs=[]
skill=[]
errorFore=[]
errorExt=[]
skill2=[]





extentPredDtT=load(linedatapath+iceType+'PredDt'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1.txt')
extentObsDtT=load(linedatapath+iceType+'ObsDt'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1.txt')
extentPredAbsT=load(linedatapath+iceType+'PredAbs'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1.txt')
anomsT=load(linedatapath+iceType+'anoms'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1.txt')
perrT=load(linedatapath+iceType+'perr'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1.txt')

skillT, errorForeT, errorExtT, skill2T = loadtxt(skilldatapath+'Skill_'+varStr+'fm'+str(fmonth)+'pm'+str(pmonth)+'R'+str(region)+str(startYearPred)+str(endYear)+'W1'+iceType+'.txt', skiprows=1,dtype='S')


fig = figure(figsize=(3.5,2.4))
ax1=subplot(2, 1, 1)
im1 = plot(years, extent, 'k', label='Observed')
#im2 = plot(Years[start_year_pred-start_year:], lineT[start_year_pred-start_year:]+ExtentG, 'r')
im2 = plot(yearsP, extentPredAbsT, 'b', linewidth=1, label='Forecast')

ax1.errorbar(yearsP[-1], extentPredAbsT[-1] , yerr=perrT[-1], color='b',fmt='',linestyle='',lw=0.6,capsize=0.5, zorder = 2)
ax1.errorbar(yearsP[-1], extentPredAbsT[-1] , yerr=1.96*perrT[-1], color='b',fmt='',linestyle='',lw=0.3,capsize=0.5, zorder = 2)

forecastStr='%0.2f' %extentPredAbsT[-1]

if (outNum==1):
	print 'outStr'
	forecastStr='%0.2f' %extentPredAbsT[-1]

	ax1.annotate(forecastStr, xy=(2018.01, extentPredAbsT[-1]-0.3), xycoords='data', 
		horizontalalignment='left', color='b', verticalalignment='middle')



#ax1.annotate(forecastStr, xy=(2018.01, extentPredAbsT[-1]-0.3), xycoords='data', 
#	horizontalalignment='left', color='b', verticalalignment='middle')

plts_net=im1+im2
#types = ['BaKa-all (2016)', 'BaKa-ice (2016)', 'BaKa-all (clim)', 'BaKa-ice (clim)']
leg = ax1.legend(loc=1, ncol=2, columnspacing=0.8, frameon=False,handletextpad=0.1, borderaxespad=0.)

#im3 = plot(yearsP, extentPredAbs[0], 'r')
#errorbar(yearsP[-1], yerr=perr[-1] , color='r',fmt='',linestyle='',lw=0.4,capsize=0.5, zorder = 2)
#ax1.errorbar(yearsP, extentPredAbs , yerr=perr, color='r',fmt='',linestyle='',lw=0.6,capsize=0.5, zorder = 2)
#ax1.errorbar(yearsP, extentPredAbs , yerr=[1.96*x for x in perr], color='r',fmt='',linestyle='',lw=0.3,capsize=0.5, zorder = 2)

ax1.annotate(regionOut, xy=(0.03, 0.03), xycoords='axes fraction', 
	horizontalalignment='left', color='k', verticalalignment='bottom')


ax1.set_ylabel('Sept ice '+iceType+r' (M km$^2$)')
ax1.set_xticks(np.arange(startYearPred, endYear+1, 4))
ax1.set_xlim(startYearPred, endYear+1)

ax1.set_xticklabels([])
ylim(ymin, ymax)
ax1.yaxis.set_major_locator(MaxNLocator(integer=True))

ax2=subplot(2, 1, 2)
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
im21 = plot(yearsP, extentObsDtT, 'k')
im3 = plot(yearsP, extentPredDtT, 'b', linewidth=1)

ax2.errorbar(yearsP[-1], extentPredDtT[-1] , yerr=perrT[-1], color='b',fmt='',linestyle='',lw=0.6,capsize=0.5, zorder = 2)
ax2.errorbar(yearsP[-1], extentPredDtT[-1] , yerr=1.96*perrT[-1], color='b',fmt='',linestyle='',lw=0.3,capsize=0.5, zorder = 2)


#ax2.errorbar(yearsP, extentPredDt , yerr=perr, color='r',fmt='',linestyle='',lw=0.6,capsize=0.5, zorder = 2)
#ax2.errorbar(yearsP, extentPredDt , yerr=[1.96*x for x in perr], color='r',fmt='',linestyle='',lw=0.3,capsize=0.5, zorder = 2)

#ax2.annotate(r'$\sigma_{anom}$='+errorExtT, 
#	xy=(0.03, 0.8), xycoords='axes fraction', horizontalalignment='left', verticalalignment='bottom')

ax2.set_ylabel(iceType+r' anomaly (M km$^2$)', rotation=270, labelpad=10)
ax2.set_xlabel('Years')
ax2.set_yticks(np.arange(yminAnom, ymaxAnom+1))
ax2.set_xticks(np.arange(startYearPred, endYear+1, 4))
ax2.set_xlim(startYearPred, endYear+1.)
ax2.axhline(0, linewidth=0.5,linestyle='--', color='k')

#ax2.annotate('Forecast skill: '+skillT, xy=(0.03, 0.96), xycoords='axes fraction', 
#	horizontalalignment='left', color='b', verticalalignment='top')
ax2.annotate('July forecast', xy=(0.03, 0.96), xycoords='axes fraction', 
	horizontalalignment='left', color='k', verticalalignment='top')

#ax2.annotate('Forecast skill (1985-2016/2008-2016): '+skillT+'/'+skill2T, xy=(0.03, 0.96), xycoords='axes fraction', 
#	horizontalalignment='left', color='k', verticalalignment='top')

subplots_adjust(left=0.12, right=0.885, bottom=0.15, top=0.96, hspace=0)

#savefig(figpath+'/forecast'+str(startYearPred)+str(endYear)+'M'+str(fmonth)+varStr+iceType+'R'+str(region)+'.pdf', dpi=300)
savefig(figpath+'/forecast'+str(startYearPred)+str(endYear)+'M'+str(fmonth)+varStr+iceType+'R'+str(region)+'.png', dpi=300)
close(fig)
