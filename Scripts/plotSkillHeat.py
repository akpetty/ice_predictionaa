############################################################## 
# Date: 01/04/17
# Name: plotSkillHeat.py
# Author: Alek Petty
# Description: Script to plot skill heat map

import matplotlib
matplotlib.use("AGG")
import pred_funcs as pfuncs
from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from scipy.io import netcdf
import numpy.ma as ma
from matplotlib import rc
from glob import glob
import matplotlib.patches as patches
from scipy.interpolate import griddata
import pandas as pd
from scipy import stats

viridis=pfuncs.get_new_cmaps(cmap_str='viridis')
rcParams['xtick.major.size'] = 2
rcParams['ytick.major.size'] = 2
rcParams['axes.linewidth'] = .5
rcParams['lines.linewidth'] = .5
rcParams['patch.linewidth'] = .5
rcParams['axes.labelsize'] = 8
rcParams['xtick.labelsize']=8
rcParams['ytick.labelsize']=8
rcParams['legend.fontsize']=8
rcParams['font.size']=8
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
skilldatapath='../Data_output/SKILL/'
#dataoutpathC='./Data_output/CONC_OUT/'
figpath='../Figures/Paper/'

endYear=2016
startYearPred=1985
skills=[]
skillsP=[]

varstr="conc"
weight=1
iceType='extent'

skill_num=0
skills=np.zeros((6, 6, 2))

for fm in xrange(8, 11):
	for pm in xrange(9, 12):

		skillC = loadtxt(skilldatapath+'Skill_'+varstrs+'fm'+str(fm)+'pm'+str(pm)+str(startYearPred)+str(endYear)+'W'+str(weight)+iceType+'.txt', skiprows=1)
	
		skills[fm-2, pm-]=skillC[0]




fig = figure(figsize=(3.5,3.8))
ax1=subplot(2, 1, 1)
im1 = plot(Concdays, skills[0, :, 0], 'o',color='b', linestyle='-', markersize=5, alpha=0.8)
im2 = plot(Concdays, skills[1, :, 0], 'v',color='b', linestyle='--', markersize=3, alpha=0.8)
im3 = plot(Concdays, skills[2, :, 0], 'o',color='r', linestyle='-', markersize=5, alpha=0.8)
im4 = plot(Concdays, skills[3, :, 0], 'v',color='r', linestyle='--', markersize=3, alpha=0.8)
im5 = plot(Concdays, skills[4, :, 0], 'o',color='g', linestyle='-', markersize=5, alpha=0.8)


subplots_adjust(left=0.13, right=0.98, top=0.96, bottom=0.1, hspace=0.1)
savefig(figpath+'skill_monthsALL'+str(skill_num)+'N1.pdf', dpi=300)
close(fig)







