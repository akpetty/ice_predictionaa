############################################################## 
# Date: 01/04/17
# Name: plotSkillHeat.py
# Author: Alek Petty
# Description: Script to plot skill heat map

import matplotlib
matplotlib.use("AGG")
import pred_funcs as pfuncs
from mpl_toolkits.basemap import Basemap, shiftgrid
import numpy as np
from pylab import *
from scipy.io import netcdf
import numpy.ma as ma
from matplotlib import rc
from glob import glob
import matplotlib.patches as patches
from scipy.interpolate import griddata
import pandas as pd
from scipy import stats

rcParams['xtick.major.size'] = 2
rcParams['ytick.major.size'] = 2
rcParams['axes.linewidth'] = .5
rcParams['lines.linewidth'] = .5
rcParams['patch.linewidth'] = .5
rcParams['axes.labelsize'] = 8
rcParams['xtick.labelsize']=8
rcParams['ytick.labelsize']=8
rcParams['legend.fontsize']=8
rcParams['font.size']=8
rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
skilldatapath='../DataOutput/SkillValsRyan/'
#dataoutpathC='./Data_output/CONC_OUT/'
figpath='../Figures/'

endYear=2016
startYearPred=1985

varstr="conc"
weight=1
iceType='extent'


skills=np.zeros((12, 12, 2))


for fm in xrange(1, 12+1):
	for pm in xrange(1, 12+1):

		skillsT=loadtxt(skilldatapath+'Skill_'+varstr+'fm'+str(fmonth)+'pm'+str(pmonth)
		+str(startYearPred)+str(endYear)+'W'+str(weight)+iceType+'.txt', 
		array([skill, errorFore, errorExt, skill2])[None], 
		header='skill error_forr error_obs skill2', fmt='%s')

		
		skills[fm-1, pm-1, 0]=skillsT[0] #long term (1985-2016) skill
		skills[fm-1, pm-1, 1]=skillsT[3] #recent (2008-2016) skill





fig = figure(figsize=(6,3))
ax1=subplot(1, 3, 1)
im1 = pcolormesh(skills1985, vmin=minval, vmax=maxval, cmap=cm.RdBu_r)
ax1.set_xticks(np.arange(0.5, 12, 1))
ax1.set_xticklabels(np.arange(1, 12+1, 1))
ax1.set_yticks(np.arange(0.5, 12, 1))
ax1.set_yticklabels(np.arange(1, 12+1, 1))
ax1.set_ylabel('pmonth')
ax1.xaxis.tick_top()
ax1.invert_yaxis()

cax = fig.add_axes([0.11, 0.1, 0.2, 0.02])
cbar = colorbar(im1,cax=cax, orientation='horizontal', extend='both', use_gridspec=True)
cbar.set_label('Extent skill',labelpad=3)
cbar.set_ticks([minval, 0, maxval])







